<?php

function _fcm_watchdog ($obj) {
	watchdog('FCM', '<pre>'.var_export($obj, true).'</pre>');
}

function _fcm_error_log($obj) {
	error_log(print_r($obj,true));
}
function _fcm_read_default_val($field, $values) {
	return (isset($values[$field])) ? $values[$field] : '';
}

function _fcm_read_type($form_state, $default = '') {
	return _fcm_read_init_val('type', $form_state, $default);
}

function _fcm_is_null_or_empty ($arr, $field) {
	return (isset($arr[$field]) && strlen($arr[$field]));
}

function _fcm_generator_read_stage_values($stage, $form_state, $default=array()) {
	return ( 
		isset($form_state['stage_values']) && 
		isset($form_state['stage_values'][$stage])
	)? $form_state['stage_values'][$stage] : $default;
}

function _fcm_read_stage_val ($field, $stage, $form_state, $default='') {
	if (isset($form_state['values']) && isset($form_state['values'][$field])) return $form_state['values'][$field]; 
	$stage_values = _fcm_generator_read_stage_values($stage, $form_state);

	return isset($stage_values[$field]) ? $stage_values[$field] : $default ;
}

function fcm_update_element_validation (&$element) {
	if (isset($element['#element_validate'])) {
		array_unshift($element['#element_validate'],'fcm_generator_validation_preload');
	}
}

function _fcm_read_init_val($field, $form_state, $default = '') {return _fcm_read_stage_val($field,'init', $form_state, $default);}

function fcm_generator_form_init($form, &$form_state) {
	$field_type_options = field_ui_field_type_options();
	$values = (isset($form_state['stage_values']) && isset($form_state['stage_values']['init']))?$form_state['stage_values']['init']:array();
	$type_selected = strlen(_fcm_read_default_val('type', $values)) || strlen(isset($form_state['values']['type']) ? $form_state['values']['type'] : '');
	$form_state_values = (isset($form_state['values'])) ? $form_state['values'] : array();

	$widget_type_options = field_ui_widget_type_options( _fcm_read_type($form_state, null), TRUE);
	$formatter_options = field_ui_formatter_options( _fcm_read_type($form_state, null));

	$form['field_name'] = array(
		'#type' => 'textfield',
		'#title' => t('New field name'),
		'#field_prefix' => '<span dir="ltr">field_',
		'#field_suffix' => '</span>&lrm;',
		'#size' => 15,
		'#description' => t('A unique machine-readable name containing letters, numbers, and underscores.'),
		// 32 characters minus the 'field_' prefix.
		'#maxlength' => 26,
		'#prefix' => '<div class="add-new-placeholder">&nbsp;</div>',
		'#default_value'=>_fcm_read_default_val('field_name', $values),
		'#required' => TRUE,
	);
 $form['field_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => _fcm_read_init_val('field_label', $form_state),
    '#required' => TRUE,
  );

 	$form['type'] = array(
		'#type' => 'select',
		'#title' => t('Type of new field'),
		'#options' => $field_type_options,
		'#empty_option' => t('- Select a field type -'),
		'#description' => t('Type of data to store.'),
		'#ajax'=>array(
			'wrapper'=>'type_dependent_container',
			'callback'=>'fcm_generator_form_init_type_selected_ajax'
		),
		'#required'=>TRUE,
		'#default_value'=>_fcm_read_default_val('type', $values),
	);

	$form['type_dependent'] = array(
		'#type'=>'container',
		'#id'=>'type_dependent_container',
		'#tree'=>FALSE
	);

	$form['type_dependent']['widget_type'] = array(
		'#type' => 'select',
		'#title' => t('Widget for new field'),
		'#options' => $widget_type_options,
		'#empty_option' => t('- Select a widget -'),
		'#description' => t('Form element to edit the data.'),
		'#disabled'=> !$type_selected,
		'#default_value'=>_fcm_read_init_val('widget_type', $form_state),
		'#required'=>TRUE,
	);

	$field_label_options = array(
		'above' => t('Above'),
		'inline' => t('Inline'),
		'hidden' => t('<Hidden>'),
	);

	$extra_visibility_options = array(
		'hidden' => t('Hidden'),
	);
	$formatter_options['hidden'] = t('<Hidden>');

	$form['type_dependent']['format'] = array(
		'#type' => 'select',
		'#title' => t('Format'),
		'#options' => $formatter_options+$extra_visibility_options,
		'#disabled'=> !$type_selected,
		'#required'=>TRUE,
		'#default_value'=>_fcm_read_default_val('format',$values),
	);

	$form['label'] = array(
		'#type' => 'select',
		'#title' => t('Label display'),
		'#options' => $field_label_options,
		'#default_value'=>_fcm_read_default_val('label', $values),
		//'#required'=>TRUE,
  );

	return $form;
}

function _fcm_generator_mimic_field_record($form_state) {
	$type = _fcm_read_type($form_state);
	$f = field_info_field_types($type);
	$init_vals = _fcm_generator_read_stage_values('init', $form_state);
	$f['type'] = $type;
	$f['field_name'] = $init_vals['field_name'];
	$f['active'] = true;

	module_load_include('install', $f['module'], $f['module']);
	$f+=((array) module_invoke($f['module'], 'field_schema', $f));
	$f['storage'] = module_invoke($f['module'], 'field_storage_info');
	$f['cardinality'] = 1;
	$f['id'] = -1;

	$settings =_fcm_generator_read_stage_values('instance_settings', $form_state);
	if (isset($settings['field']) && isset($settings['field']['settings'])){
		$f['settings'] = $settings['field']['settings'] + $f['settings'];
	}
	return $f;
}

function _fcm_generator_mimic_instance ($form_state) {
	$gen_info = _field_info_collate_types();



	///TODO: prodji detaljno kroz strukture za instancu i tipove i utegni ovo ....
	$init_stage = _fcm_generator_read_stage_values('init', $form_state);


	$fit = field_info_field_types($init_stage['type']);


	///instance settings issue ....
	$instance_settings=_fcm_generator_read_stage_values('instance_settings', $form_state, $fit['instance_settings']);
	if (!isset($instance_settings)) $instance_settings = array();
	$field_info_hook = module_invoke($fit['module'], 'field_info');
	if (isset($fit['property_type'])) {
	$fis = (isset($field_info_hook[$fit['property_type']]) && isset($field_info_hook[$fit['property_type']]['instance_settings'])) ? 
		$field_info_hook[$fit['property_type']]['instance_settings']:array();
	}else{
		$fis = array();
	}

	$fis+= $instance_settings;

	$field_settings = (isset($fis['field']) && isset($fis['field']['settings'])) ? $fis['field']['settings'] : array();
	$field_settings+=$fis;
	
	//widget
	$default_widget_info = $gen_info['widget types'];

	$selected_widget = _fcm_is_null_or_empty($init_stage, 'widget_type') ? $init_stage['widget_type'] : $fit['default_widget'];
	$formatter = _fcm_is_null_or_empty($init_stage, 'format') ? $init_stage['format'] : $fit['default_formatter'];

	$ffi = $gen_info['formatter types'];//module_invoke($fit['module'], 'field_formatter_info');
	$widget_config = ((isset($default_widget_info[$selected_widget])) ? $default_widget_info[$selected_widget]: array())+array(
		'type'=>$selected_widget,
		'module'=>$fit['module'],
		'weight'=>'10',  ///ZBUDZ
	);


	$ret = array(
		'field_name'=>$init_stage['field_name'],
		'label'=>$init_stage['field_label'],
		'description'=>'', //do not know that .... at field settings widget, but could read from it ....
		'entity_type'=>'',
		'settings'=>$field_settings,
		'bundle'=>'',
		'default_value'=>null,

		'widget'=>$widget_config,
		'display'=>array(
			'label'=>$init_stage['label'],
			'type'=> $formatter,
			'settings'=>$ffi[$formatter]
		),
	);
	
	_fcm_watchdog(array(
		module_invoke($fit['module'],'field_info'),
		module_invoke($fit['module'],'field_formatter_info'),
		module_invoke($fit['module'],'field_widget_info'),
	));
	return $ret;
}

/*
function fcm_generator_form_field_settings_bogus($form, &$form_state) {
	$field = _fcm_generator_mimic_field_record($form_state);
	$instance = _fcm_generator_mimic_instance($form_state);

	drupal_set_title($instance['label']);

	$description = '<p>' . t('These settings apply to the %field field everywhere it is used. These settings impact the way that data is stored in the database and cannot be changed once data has been created.', array('%field' => $instance['label'])) . '</p>';

	// Create a form structure for the field values.
	$form['field'] = array(
		'#type' => 'fieldset',
		'#title' => t('Field settings'),
		'#description' => $description,
		'#tree' => TRUE,
	);

	// See if data already exists for this field.
	// If so, prevent changes to the field settings.
	$has_data = false;
	// Build the non-configurable field values.
	$form['field']['field_name'] = array('#type' => 'value', '#value' => $field['field_name']);
	$form['field']['type'] = array('#type' => 'value', '#value' => $field['type']);
	$form['field']['module'] = array('#type' => 'value', '#value' => $field['module']);
	$form['field']['active'] = array('#type' => 'value', '#value' => $field['active']);

	// Add settings provided by the field module. The field module is
	// responsible for not returning settings that cannot be changed if
	// the field already has data.
	$form['field']['settings'] = array();
	$additions = module_invoke($field['module'], 'field_settings_form', $field, $instance, $has_data);
	if (is_array($additions)) {
		if (isset($additions['#element_validate'])){
			array_unshift($additions['#element_validate'], 'fcm_generator_form_validate');
		}
		$form['field']['settings'] = $additions;
	}
	if (empty($form['field']['settings'])) {
		$form['field']['settings'] = array(
			'#markup' => t('%field has no field settings.', array('%field' => $instance['label'])),
		);
	}

	$form['actions'] = array('#type' => 'actions');
	return $form;

}
*/

function fcm_generator_form_instance_settings ($form, &$form_state) {
  $field = _fcm_generator_mimic_field_record($form_state);
	$instance = _fcm_generator_mimic_instance($form_state);

  drupal_set_title($instance['label']);

  $form['#field'] = $field;
  $form['#instance'] = $instance;

  $field_type = field_info_field_types($field['type']);
  $widget_type = field_info_widget_types($instance['widget']['type']);
  $bundles = field_info_bundles();

  // Create a form structure for the instance values.
  $form['instance'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#description' => t('These settings apply only to the %field field.', array(
      '%field' => $instance['label'],
    )),
    // Ensure field_ui_field_edit_instance_pre_render() gets called in addition
    // to, not instead of, the #pre_render function(s) needed by all fieldsets.
    '#pre_render' => array_merge(array('field_ui_field_edit_instance_pre_render'), element_info_property('fieldset', '#pre_render', array())),
  );

  // Build the non-configurable instance values.
  $form['instance']['field_name'] = array(
    '#type' => 'value',
    '#value' => $instance['field_name'],
  );
  $form['instance']['widget']['weight'] = array(
    '#type' => 'value',
    '#value' => !empty($instance['widget']['weight']) ? $instance['widget']['weight'] : 0,
  );

  // Build the configurable instance values.
  $form['instance']['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => !empty($instance['label']) ? $instance['label'] : $field['field_name'],
    '#required' => TRUE,
    '#weight' => -20,
  );
  $form['instance']['required'] = array(
    '#type' => 'checkbox',
    '#title' => t('Required field'),
    '#default_value' => !empty($instance['required']),
    '#weight' => -10,
  );

  $form['instance']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Help text'),
    '#default_value' => !empty($instance['description']) ? $instance['description'] : '',
    '#rows' => 5,
    '#description' => t('Instructions to present to the user below this field on the editing form.<br />Allowed HTML tags: @tags', array('@tags' => _field_filter_xss_display_allowed_tags())),
    '#weight' => -5,
  );

  // Build the widget component of the instance.
  $form['instance']['widget']['type'] = array(
    '#type' => 'value',
    '#value' => $instance['widget']['type'],
  );
  $form['instance']['widget']['module'] = array(
    '#type' => 'value',
    '#value' => $widget_type['module'],
  );
  $form['instance']['widget']['active'] = array(
    '#type' => 'value',
    '#value' => !empty($field['instance']['widget']['active']) ? 1 : 0,
  );

  // Add additional field instance settings from the field module.
  $additions = module_invoke($field['module'], 'field_instance_settings_form', $field, $instance);
	if (is_array($additions)) {
		fcm_update_element_validation($additions);
    $form['instance']['settings'] = $additions;
  }

  // Add additional widget settings from the widget module.
  $additions = module_invoke($widget_type['module'], 'field_widget_settings_form', $field, $instance);
	if (is_array($additions)) {
		fcm_update_element_validation($additions);
    $form['instance']['widget']['settings'] = $additions;
    $form['instance']['widget']['active']['#value'] = 1;
  }

	// Add handling for default value if not provided by any other module.
	/*
  if (field_behaviors_widget('default value', $instance) == FIELD_BEHAVIOR_DEFAULT && empty($instance['default_value_function'])) {
    $form['instance']['default_value_widget'] = field_ui_default_value_widget($field, $instance, $form, $form_state);
	}
	 */

  $has_data = false;
	$description = '<p>' . t('These settings apply to the %field field everywhere it is used.', array('%field' => $instance['label'])) . '</p>';

  // Create a form structure for the field values.
  $form['field'] = array(
    '#type' => 'fieldset',
    '#title' => t('%field field settings', array('%field' => $instance['label'])),
    '#description' => $description,
    '#tree' => TRUE,
  );

  // Build the configurable field values.
  $description = t('Maximum number of values users can enter for this field.');
  if (field_behaviors_widget('multiple values', $instance) == FIELD_BEHAVIOR_DEFAULT) {
    $description .= '<br/>' . t("'Unlimited' will provide an 'Add more' button so the users can add as many values as they like.");
  }
  $form['field']['cardinality'] = array(
    '#type' => 'select',
    '#title' => t('Number of values'),
    '#options' => array(FIELD_CARDINALITY_UNLIMITED => t('Unlimited')) + drupal_map_assoc(range(1, 10)),
    '#default_value' => $field['cardinality'],
    '#description' => $description,
  );

  // Add additional field type settings. The field type module is
  // responsible for not returning settings that cannot be changed if
  // the field already has data.
  $additions = module_invoke($field['module'], 'field_settings_form', $field, $instance, $has_data);
  if (is_array($additions)) {
		fcm_update_element_validation($additions);
    $form['field']['settings'] = $additions;
	}
  return $form;
}


function fcm_generator_form_default_settings($form, &$form_state) {
	$field = _fcm_generator_mimic_field_record($form_state);
	$instance = _fcm_generator_mimic_instance($form_state);
	if (field_behaviors_widget('default value', $instance) == FIELD_BEHAVIOR_DEFAULT && empty($instance['default_value_function'])) {
		$form['default_value_widget'] = field_ui_default_value_widget($field, $instance, $form, $form_state);
	}
	return $form;

}

function fcm_generator_form_data_review($form, &$form_state) {
	$sv = $form_state['stage_values'];
	foreach ($sv as $stage=>$data) {
		foreach(array('back', 'next', 'form_build_id', 'form_token', 'form_id', 'op') as $rem) {
			unset($sv[$stage][$rem]);
		}
	}

	$field_name = _fcm_read_init_val('field_name', $form_state);
	if (!isset($form_state['field_data'])) $form_state['field_data'] = array();
	$form_state['field_data'][$field_name] = $sv;



	$form['data'] = array(
		'#type'=>'markup',
		'#markup'=>'<pre>'.var_export($sv, true).'</pre></br>',
	);
	return $form;
}

function fcm_generator_form_init_type_selected_ajax ($form, $form_state) {
	return $form['type_dependent'];
}

function fcm_generator_next_stage($stage){
	switch($stage) {
	case 'init': return 'instance_settings';
	case 'instance_settings': return 'default_settings';
	case 'default_settings':return 'data_review';
	case 'data_review': return 'init';
	default:return 'init';
	}
}

function fcm_generator_previous_stage($stage){
	switch($stage) {
	case 'data_review': return 'default_settings';
	case 'default_settings': return 'instance_settings';
	default:return 'init';
	}
}

function fcm_generator_form_submit($form, &$form_state) {
	$next_stage;
	$stage = $form_state['stage'];
	if ($form_state['triggering_element']['#value'] == 'Next') {
		$next_stage = fcm_generator_next_stage($stage);
	}
	if ($form_state['triggering_element']['#value'] == 'Back') {
		unset($form_state['stage_values'][$stage]);
		$next_stage = fcm_generator_previous_stage($stage);
	}

	if (isset($form_state['multistep_values']['form_build_id'])) {
		$form_state['values']['form_build_id'] = $form_state['multistep_values']['form_build_id'];
	}
	$form_state['multistep_values']['form_build_id'] = $form_state['values']['form_build_id'];
	$form_state['stage'] = $next_stage;
	if(!isset($form_state['stage_values'])) $form_state['stage_values'] = array();
	$form_state['stage_values'][$stage] = $form_state['values'];


	if ($stage == 'default_settings' && isset($form['default_value_widget'])) {
		$field = _fcm_generator_mimic_field_record($form_state);
		$instance = _fcm_generator_mimic_instance($form_state);
		$element = $form['default_value_widget'];
		$items = array();

		$path = array($field['field_name'], LANGUAGE_NONE);
		$values = drupal_array_get_nested_value($form_state['values'], $path);
		foreach ($values as $i=>$v) {
			foreach ($v as $key=>$val) {
				if (empty($val)) {
					$values[$i][$key] = null;
				}
			}
		}

		$form_state['stage_values'][$stage] = $values;
	}

	if ($next_stage == 'init' && $stage == 'data_review') {
		$fds = $form_state['stage_values']; //field data structure
		$field_name = $fds['instance_settings']['instance']['field_name'];

		$dv = $fds['default_settings'];

		$ts = array( //to set ...
			'type'=>$fds['init']['type'],
			'label'=>$fds['instance_settings']['instance']['label'],
			'widget'=>$fds['instance_settings']['instance']['widget'],
			'settings'=>$fds['instance_settings']['field']['settings'],
			'description'=>$fds['instance_settings']['instance']['description'],
			'required'=>$fds['instance_settings']['instance']['required'],
			'default_value'=>$dv,
			'field_config'=>array(
				'active'=>$fds['instance_settings']['instance']['widget']['active'],
				'field_name'=>$field_name,
			),
			'cardinality'=>$fds['instance_settings']['field']['cardinality'],
		);

		if (!isset($form_state['fds'])) $form_state['fds'] = array();
		$form_state['fds'][$field_name] = $ts;
		unset($form_state['stage_values']);
	}

	$form_state['rebuild'] = TRUE;
}	

function fcm_generator_form ($form, &$form_state) {
	module_load_include('inc', 'field_ui', 'field_ui.admin');
	$stage = (isset($form_state['stage']))?$form_state['stage']:'init'; 

	$stage_labels = array(
		'init'=>t('Basic field info'),
		'instance_settings'=>t('Instance settings'),
		'data_review'=>t('Data review'),
		'default_settings'=>t('Default settings')
	);

	$f = 'fcm_generator_form_'.$stage;
	$form['stage_display'] = array(
		'#markup'=>'STAGE: '.$stage_labels[$stage],
	);

	if (function_exists($f)) $form += call_user_func($f, $form, $form_state);
	$form_state['stage'] = $stage;


	$form['back'] = array(
		'#type'=>'submit',
		'#value'=>t('Back'),
		'#disabled'=>($stage == 'init'),
		'#weight'=>100,
	);

	$form['next'] = array(
		'#type'=>'submit',
		'#value'=>t('Next'),
		'#weight'=>100,
	);


	if (isset($form_state['fds'])) {
		$form['code'] = array(
			'#markup' => '<pre>'.var_export($form_state['fds'], true).'</pre>',
		);
	}

	return $form;
}

function fcm_generator_validation_preload($form, &$form_state) {
	if ($form_state['stage'] == 'init') return;
	$field = _fcm_generator_mimic_field_record($form_state);
	$instance = _fcm_generator_mimic_instance($form_state);
	//this is stupid, but I do not know how field_ui does it ... so, invoke these hooks just in case someone has something to validate
	module_invoke($field['module'], 'field_settings_form', $field,$instance, false);
  module_invoke($field['module'], 'field_instance_settings_form', $field, $instance);
  $widget_type = field_info_widget_types($instance['widget']['type']);
	module_invoke($widget_type['module'], 'field_widget_settings_form', $field, $instance);

}
?>
